# Contributing

The best way to contribute changes to MongoRest is to fork the GitLab project,
make the changes local to your forked repository, then submit a Merge Request
through GitLab.

Other than typical Ruby code formatting enforced through
[rubocop](https://github.com/rubocop-hq/rubocop), there are no formal coding or
writing standards used within MongoRest.  That said, your Merge Request is more
likely to be successful integrated if your changes are consistent with the
existing styles.  This includes indentations, variable/method names, and even
the amount of comments and documentation.

Merge Requests are also more likely to be successful if the commits also include
new tests to ensure the new or fixed functionality actually works.  Though there
are _some_ use cases where more tests will not be necessary.  This will be
determined on a case-by-case basis.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run
`rake test` to run the tests. You can also run `bin/console` for an interactive
prompt that will allow you to experiment.  Or finally, you can run `bin/run` to
start a new MongoRest server running on the default IP/port (`localhost:29017`).

## Committing a new version

The commands below are intended for the the true maintainers of MongoRest, i.e.
anyone given maintainer/developer rights on the GitLab project.

To install this gem onto your local machine, run `bundle exec rake install`.  It
is recommended you install the gem locally and run any final testing before
publishing the gem.

If you believe the gem is production worthy, you can release a new version with:

```bash
# Insall bumpversion.
pip3 install --user bumpversion

# Increment the version number.
bumpversion patch
# or "bumpversion minor", if it's appropriate!
# or "bumpversion major", if it's appropriate!!

# Create a Git tag, push commits to the remote, and publish the Gem.
bundle exec rake release
```
