# MongoRest

[![pipeline](https://gitlab.com/deliberist/mongo_rest/badges/master/pipeline.svg)](https://gitlab.com/deliberist/mongo_rest/pipelines)
[![Gem Version](https://badge.fury.io/rb/mongo_rest.png)](http://badge.fury.io/rb/mongo_rest)

Welcome to the home of MongoRest, a ReSTful API for a
[MongoDB](https://mongodb.com/) instance.

Currently the ReSTful API exposes the simplest of MongoDB operations.  That is,
operations that only take a single _filter_ argument.  MongoRest may be updated
in the future to support MongoDB operations that accept more than one argument.
But for the time being, MongoRest has the ability to create, query, and delete
documents.

- [But, why?](#but-why)
- [Usage](#usage)
  - [Constructing Filters](#constructing-filters)
  - [Constructing the URL/URI](#constructing-the-urluri)
  - [Clients](#clients)
    - [Browser](#browser)
    - [cURL](#curl)
  - [Authentication](#authentication)
  - [Authorization](#authorization)
- [Install and Run](#install-and-run)
- [Config File Options](#config-file-options)
- [License](#license)

## But, why?

Why would you want to expose your database over an HTTP interface?  There are
some use cases where client apps only speak HTTP, and not the MongoDB
[wire protocol](https://docs.mongodb.com/manual/reference/mongodb-wire-protocol)
requires.  This could be for any number of reasons, maybe the language of your
project does not yet have a native MongoDB driver.  Maybe your database is
behind a network firewall that terminates the MongoDB wire protocol, but HTTP is
passed through.  Or maybe more simply, you need an HTTP interface to help!
facilitate testing.  Whatever the reason, MongoRest is here to (hopefully) help!

In case this is not obvious, extreme care should be taken when exposing your
database beyond what is required.  This includes opening up production
databases to something like an HTTP interface -- eg. MongoRest.  In theory, this
allows a simple browser to act as an attack tool against what could be sensitive
information in your database.

Although it could be, MongoRest is not intended to be a production ready
solution.  You should make a valiant effort to use the most secure solutions.
If that includes MongoRest, then great!

## Usage

### Constructing Filters

Since MongoRest exposes a handful of HTTP endpoints, essentially MongoRest just
needs the request method (e.g. HTTP verb), database name, collection name,
operation name, and arguments to the operation.  Most are encoded into the URI
itself.

For example, suppose we want to query for documents that contain
`name: Fred Flintstone`.  The typical JSON document may look like:

```json
{"name": "Fred Flintstone"}
```

We first need to encode the JSON document for a URI, which the document will
become: `%7B%22name%22%3A%20%22Fred%20Flintstone%22%7D`.  We can then construct
the URI endpoint variables as:

- `?filter=%7B%22name%22%3A%20%22Fred%20Flintstone%22%7D`

There are many libraries and services that can encode/decode URLs.  For a Ruby
implementation, check out
[CGI.escape](https://ruby-doc.org/stdlib-2.4.3/libdoc/cgi/rdoc/CGI/Util.html#method-i-escape)

```ruby
require 'cgi'
doc = '{"name": "balls"}'
filter = CGI.escape(doc)  # "%7B%22name%22%3A+%22balls%22%7D"
```

To put this together into a URL, assuming MongoRest is running on the default
IP/port, the URL becomes:

- `http://localhost:29017/mydb/mycoll/find?filter=%7B%22name%22%3A%20%22Fred%20Flintstone%22%7D`

### Constructing the URL/URI

Database CRUD operations follow the naming conventions from MongoDB's Ruby
driver, specifically the operations defined in the
[Mongo::Collection](https://api.mongodb.com/ruby/current/Mongo/Collection.html)
class.

These methods are then validated against HTTP verbs to make sure they are used
appropriately.  This table depicts which operations are available for each HTTP
verb:

| HTTP Verb | MongoRest Operation | API Endpoint Example |
|-----------|---------------------|----------------------|
| GET       | aggregate           | /mydb/mycoll/aggregate?filter={} |
| GET       | count_documents     | /mydb/mycoll/count_documents?filter={} |
| GET       | find                | /mydb/mycoll/find?filter={} |
| GET       | find_one_and_delete | /mydb/mycoll/find_one_and_delete?filter={} |
| POST      | insert_many         | /mydb/mycoll/insert_many?filter=[] |
| POST      | insert_one          | /mydb/mycoll/insert_one?filter={} |
| DELETE    | delete_many         | /mydb/mycoll/delete_many?filter={} |
| DELETE    | delete_one          | /mydb/mycoll/delete_one?filter={} |
| DELETE    | find_one_and_delete | /mydb/mycoll/find_one_and_delete?filter={} |
| PATCH     | This verb is not yet supported. | N/A |
| PUT       | This verb is not yet supported. | N/A |

Endpoint variables:

- `filter`: the filter JSON document, which may need to be encoded for the URL.
- `f`: a synonym for `filter`.

### Clients

TODO

#### Browser

TODO

#### cURL

TODO

```bash

curl http://localhost:29017/

curl http://localhost:29017/ -H "Accept: application/json" -H "Content-Type: application/json"


curl http://localhost:29017/ \
    --request POST \
    --header "Accept: application/json" \
    --header "Content-Type: application/json" \
    --data '{"username":"xyz","password":"xyz"}'


curl http://localhost:8080/mydb/mycoll/find \
    --include \
    --request GET \
    --header "Accept: application/json" \
    --header "Content-Type: application/json" \
    --header 'X-MongoDB-Doc: {"name": {"$regex": "joe", "$options": "i"}}'

http://localhost:8080/mydb/mycoll/insert_one?q={%22name%22:%22Fred%20Flintstone%22}
http://localhost:8080/mydb/mycoll/insert_one?q={%22name%22:%22Wilma%20Flintstone%22}
http://localhost:8080/mydb/mycoll/find?q={}

http://localhost:8080/mydb/mycoll/find?q={%22name%22:%22Fred%20Flintstone%22}

curl 'http://localhost:8080/mydb/mycoll/insert_one?q={"name":"Fred Flintstone"}'

curl 'http://localhost:29017/mydb/mycoll/insert_one?q=%7B%22name%22%3A%22balls%22%7D' --request POST --header "Accept: application/json" --header "Content-Type: application/json" --data ''

curl 'http://localhost:29017/mydb/mycoll/delete_many?q=%5B%7B%22name%22%3A%22balls%22%7D%2C%20%7B%22name%22%3A%22Joe%20Smith%22%7D%5D' --request DELETE --header "Accept: application/json" --header "Content-Type: application/json" --data ''
```

### Authentication

This feature is not yet implemented.

### Authorization

This feature is not yet implemented.

## Install and Run

MongoRest is published to [Ruby Gems](https://rubygems.org/gems/mongo_rest), and
thus can be easily installed via `gem`:

```bash
gem install mongodb_rest
```

The `mongo_rest` command should be placed into a directory on your `${PATH}`
just like other runnable gems.

Running `mongo_rest` for the first time will place a new config file into its
application specific XDG spec config directory.  Typically this is under the
name `${XDG_CONFIG_HOME}/mongo_rest/config.yml`.

## Config File Options

The default MongoRest config file is saved under the XDG config directory
`${XDG_CONFIG_HOME}/mongo_rest/config.yml` on the first invocation of MongoRest.
If the file already exists, MongoRest will read the existing file.

It should be noted that if `${XDG_CONFIG_HOME}` is not set, it will default to
`${HOME}/.config`.

The default config file will look something like this:

```yaml
---
:server:

  # The interface or IP to bind to as an HTTP server.
  :interface: localhost

  # The port to bind to as an HTTP server.
  :port: 29017

:database:
  
  # The URL of the MongoDB instance MongoRest will expose over HTTP.
  :url: mongodb://127.0.0.1:27017/
```

## License

The gem is available as open source under the terms of the
[MIT License](https://opensource.org/licenses/MIT).
