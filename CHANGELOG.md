# CHANGELOG.md

## Version 0

### Version 0.0.1 - not yet released.

- Added logo image files.
- Added extensive documentation in the form of a README.

### Version 0.0.0 - released 2020-June-01

- Initial version of the code which only exposes a very rudimentary API.
- Code documentation that describes the initial code.
