# frozen_string_literal: true

require 'singleton'

module MongoRest
  # Encapsulates client authentication and authorization utilities.
  class Security
    # Is singleton because this should not re-read the system's trusted certs.
    include Singleton

    # Determines if the cert can be trusted.
    def authenticated?(cert) # rubocop:disable Lint/UnusedMethodArgument
      # @todo Assert cert is trusted.
      true
    end

    # Determines if the client cert has the authorization to access the
    # specified database.
    def authorized?(cert, database_name) # rubocop:disable Lint/UnusedMethodArgument
      # @todo assert cert CN is authorized to access database, but how to make that configurable?
      true
    end
  end
end
