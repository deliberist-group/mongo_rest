# frozen_string_literal: true

require 'mongo'
require 'singleton'

require 'mongo_rest/config'

module MongoRest
  # Exposes CRUD operations on a MongoDB client connections.
  class Database
    # This is a singleton because we only need one client connection for thes
    # time being.
    include Singleton

    # Constructs a new MongoDB client.
    def initialize
      super
      url = MongoRest::Config.instance.get :database, :url
      @client = Mongo::Client.new url
    end

    # Runs a CRUD operation on the specified db/collection, then returns the
    # resulting JSON document(s).
    #
    # The return value is always an array, albeit empty, size of one, or an
    # indeterminate size.
    def run(database_name, collection_name, operation_name, *args)
      database = @client.use database_name
      collection = database[collection_name]
      results = collection.send(operation_name, *args)
      docs = []
      results.each do |document|
        # puts document
        docs.append document
      end
      docs
    end

    # Determines if the specified database operation is valid in the context of
    # the supplied HTTP Verb.  In general, the verbs contain these kinds of
    # CRUD operations:
    #   GET = read documents
    #   POST = create documents
    #   PATCH = modifies documents - not yet supported, requires multi-arg ops.
    #   PUT = replaces documents - not yet supported, requires multi-arg ops.
    #   DELETE = deletes documents
    def valid_op?(http_verb, operation_name) # rubocop:disable Metrics/MethodLength
      ops = {
        'GET' => %w[aggregate \
                    count_documents \
                    find \
                    find_one_and_delete],
        'POST' => %w[insert_many \
                     insert_one],
        'DELETE' => %w[delete_many \
                       delete_one \
                       find_one_and_delete]
      }
      ops[http_verb].include? operation_name
    end
  end
end
