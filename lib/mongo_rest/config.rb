# frozen_string_literal: true

require 'fileutils'
require 'singleton'
require 'xdg'
require 'safe_yaml'

module MongoRest
  # Utility class that enables easy access to the MongoRest config file.
  class Config
    # Is a singleton because this should only read the disk once.
    include Singleton

    # Configure SafeYAML!
    # SafeYAML::OPTIONS[:default_mode] = :safe
    # SafeYAML::OPTIONS[:deserialize_symbols] = false

    # Retrieves the specified property from the config file's data.
    def get(property, *rest)
      @properties.dig property, *rest
    end

    private

    # Initializes with either the contents of an existing config file or the
    # default values.
    def initialize
      super
      config_dir = File.expand_path(File.join(XDG::Environment.new.config_home,
                                              'mongo_rest'))
      FileUtils.mkdir_p config_dir
      config_file = File.join(config_dir, 'config.yml')
      @properties = if File.exist? config_file
                      parse config_file
                    else
                      use_defaults config_file
                    end
    end

    # Parses the config file from disk, and returns said properties.
    def parse(config_file)
      puts "Loading existing properties from: #{config_file}"
      YAML.load_file config_file
    end

    # Writes the default config file with the default values, then returns said
    # default properties.
    def use_defaults(config_file)
      puts "Writing default properties to: #{config_file}"
      dp = default_properties
      File.open(config_file, 'w') do |out|
        YAML.dump(dp, out)
      end
      dp
    end

    # The default configuration.
    def default_properties
      # This seems better as a class or instance variable, but I cannot seem to
      # get that to work as a singleton.
      {
        'server': {
          'interface': 'localhost',
          'port': 29_017
        },
        'database': {
          'url': 'mongodb://127.0.0.1:27017/'
        }
      }.freeze
    end
  end
end
