# frozen_string_literal: true

require 'json'
require 'sinatra/base'
require 'sinatra/json'
require 'sinatra/multi_route'

require 'mongo_rest/config'
require 'mongo_rest/database'
require 'mongo_rest/security'

module MongoRest
  # Runs a new Sinatra server to accept database CRUD operations.
  class Server < Sinatra::Base
    register Sinatra::MultiRoute

    # Read server properties from the config file.
    config = MongoRest::Config.instance
    interface = config.get :server, :interface
    port = config.get :server, :port

    # Set up the Sinatra config.
    set :bind, interface
    set :port, port
    enable :dump_errors
    enable :threaded

    # Initialize the database.
    db = MongoRest::Database.instance

    # Authenticate and authorize the clients
    before do
      # @todo pull cert from headers
      cert = nil
      halt 401, "{'error': 'Cannot authenticate client.'}" \
          unless MongoRest::Security.instance.authenticated? cert

      database_name = request.env['REQUEST_PATH'].split('/')[1]
      # puts "Database name: #{database_name}"
      halt 403, "{'error': 'Client unauthorized to access database: #{database_name}.'}" \
          unless MongoRest::Security.instance.authorized? cert, database_name
    end

    # Generic catch all for most database queries.
    # get '/:database/:collection/:operation' do
    route :get, :post, :delete, '/:database/:collection/:operation' do
      # Validate the operation.
      http_verb = request.env['REQUEST_METHOD'].upcase
      operation_name = params['operation']
      db.valid_op?(http_verb, operation_name) || \
        halt(405, "{'error': 'Invalid request method (#{http_verb}) for operation (#{operation_name})'}")

      # Find the query parameters.
      database_name = params['database']
      collection_name = params['collection']
      filter_doc = JSON.parse(params['qa1'] || params['query_arg_1'])

      # Run the operation.
      result = db.run database_name, collection_name, operation_name, filter_doc

      # "Send it!"
      #   -- Tucker Gott.
      #     -- My kid.
      json result
    end

    # Run the server.
    run!
  end
end
