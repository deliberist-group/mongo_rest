# frozen_string_literal: true

module MongoRest
  # Saves the modules version number.
  VERSION = '0.0.0'
end
