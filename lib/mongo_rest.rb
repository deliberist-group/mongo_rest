# frozen_string_literal: true

require 'mongo_rest/server'

# This module encapsulates everything pertaining to the exposure of a ReSTful
# interface to execute MongoDB CRUD operations.
module MongoRest
  # Just run a new server.
  MongoRest::Server.new
end
