# TODO

Add authentication!
- Use a `before` statement with Sinatra.
- http://sinatrarb.com/faq.html#auth

Add a logger.
- Make the normal Sinatra output a bit more minimalistic.

Add robust documentation.
- For example, the X-MongoDB-Doc requires valid JSON, so inline regex patterns
    are unacceptable.  These situations require the `$regex` operator.
    - https://docs.mongodb.com/manual/reference/operator/query/regex/
- The resulting JSON will always be an array, 0, 1, or N elements in size.
- For the most robust capabilities, use the aggregation framework in the query.
- Can query with X-MongoDB-DOC header, or the `q` URL "get-param".

Unit testing!

Bump version!

GitLab CI
- Lint Ruby files
- Run unit tests

Add the ability to run operations that require more than one argument.
- Does that mean the "query doc" should be an array that is exanded in the API route?
- Or should there be multiple GET variables? (or headers!)

Perform some validation on the user input.
- database name
- collection name
- operation name
- the filter doc
